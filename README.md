# Mon plugin pour les inscriptions d'alternance!

Ce projet a été réalisé dans le cadre d'une évaluation pour ma formation de Développeur web & web mobile.

# Setup

    Prérequis: avoir docker-compose et docker installés

**First** : git clone https://gitlab.com/flojarno/alternance-plugin-wp.git

**Second** : cd alternance-plugin-wp .

**Third** : Open a terminal and exec : docker-compose up.

> **Note:** Visit your localhost:port\* and complete wordpress installation.

**Fourth** : Create new folder 'form-alternance' in 'wordpress/wp-content/plugins/'

